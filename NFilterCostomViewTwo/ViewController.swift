//
//  ViewController.swift
//  NfilterCustomViewTwo
//
//  Created by Eunchan Kim on 2023/07/07.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var keyPad: UIView!
//    var isShowStackView:Bool =
    var isShowLabel:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showCustomView2()
    }
    
    func showCustomView1(){
        //customView 만들기
        let customView = UIView(frame: CGRect(x: 0, y: -220, width: UIScreen.main.bounds.width, height: 220))
        customView.backgroundColor = .black
        customView.layer.cornerRadius = 24
        customView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        customView.clipsToBounds = true
        
        let closeButton = UIButton(type: .custom)
        closeButton.setTitle("닫기", for: .normal)
        closeButton.titleLabel?.textAlignment = .left
        closeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        closeButton.tintColor = .white
        customView.addSubview(closeButton)
        closeButton.snp.makeConstraints{ make in
            make.trailing.equalTo(customView.snp.trailing).offset(-24)
            make.top.equalTo(customView.snp.top).offset(24)
        }
        
        let label = UILabel()
        label.text = "간편비밀번호 입력"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        customView.addSubview(label)
        label.snp.makeConstraints { make in
            make.centerX.equalTo(customView.snp.centerX)
            make.top.equalTo(closeButton.snp.bottom).offset(32)
        }
        
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.alignment = .center
        
        customView.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.centerX.equalTo(customView.snp.centerX)
            make.top.equalTo(label.snp.bottom).offset(20)
            
            // 이미지 뷰 추가
            for _ in 1...6 {
                let imageView = UIImageView()
                imageView.image = UIImage(systemName: "circle")
                imageView.tintColor = .white
                imageView.snp.makeConstraints { make in
                    make.width.height.equalTo(18)
                }
                stackView.addArrangedSubview(imageView)
            }
        }
        
        
        //기본
        let lockImg = UIImageView()
        lockImg.image = UIImage(named: "lock")
        customView.addSubview(lockImg)
        lockImg.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).offset(40)
            make.leading.equalTo(customView.snp.leading).offset(24)
        }
        
        let lockLabel = UILabel()
        lockLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
        lockLabel.textColor = .white
        lockLabel.text = "보안키보드 작동 중"
        customView.addSubview(lockLabel)
        lockLabel.snp.makeConstraints{ make in
            make.leading.equalTo(lockImg.snp.trailing).offset(8)
            make.top.equalTo(stackView.snp.bottom).offset(40)
            
        }
        keyPad.addSubview(customView)
    }
    func showCustomView2(){
        //customView 만들기
        let customView = UIView(frame: CGRect(x: 0, y: -70, width: UIScreen.main.bounds.width, height: 70))
        customView.backgroundColor = .black
        customView.layer.cornerRadius = 24
        customView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        customView.clipsToBounds = true
        
        let closeButton = UIButton(type: .custom)
        closeButton.setTitle("닫기", for: .normal)
        closeButton.titleLabel?.textAlignment = .left
        closeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        closeButton.tintColor = .white
        customView.addSubview(closeButton)
        closeButton.snp.makeConstraints{ make in
            make.trailing.equalTo(customView.snp.trailing).offset(-24)
            make.top.equalTo(customView.snp.top).offset(24)
        }

        //기본
        let lockImg = UIImageView()
        lockImg.image = UIImage(named: "lock")
        customView.addSubview(lockImg)
        lockImg.snp.makeConstraints { make in
            make.centerY.equalTo(closeButton.snp.centerY)
            make.leading.equalTo(customView.snp.leading).offset(24)
        }
        
        let lockLabel = UILabel()
        lockLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
        lockLabel.textColor = .white
        lockLabel.text = "보안키보드 작동 중"
        customView.addSubview(lockLabel)
        lockLabel.snp.makeConstraints{ make in
            make.leading.equalTo(lockImg.snp.trailing).offset(8)
            make.centerY.equalTo(closeButton.snp.centerY)
        }
        keyPad.addSubview(customView)
    }
    
}

